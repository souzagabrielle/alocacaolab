<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Alocação de laboratório </title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/heroic-features.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="index.html">Alocação de laboratorio</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="index.html">Início
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="sobre.html">Sobre</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="contato.php">Contato</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="login.php">Login </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

      <!-- Jumbotron Header -->
      <header class="jumbotron my-8">
      
      <div style="text-align: center;">
        <h1 class="display-4">Entre em contato conosco caso tenha dúvidas ou problemas</h1>
      </div>

      </header>

  <!-- Page Content -->
  <div class="container">



    <!-- Page Features -->

    <form method="post" action="contato.php">
        <div class="form-group">
            Seu email: <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Email">
        </div>
  
        <div class="form-group">
          <label for="assunto">Assunto</label>
          <input type="text" class="form-control" id="assunto" placeholder="Assunto">
        </div>

        <div class="form-group">
          <label for="assunto">Mensagem</label>
          <textarea type="textarea" class="form-control" id="assunto" placeholder="Escreva aqui sua mensagem..."> </textarea>
        </div>
  

  
       <button type="submit" class="btn btn-primary">Enviar</button>
    </form>

    <br>
    <br>
    <!-- /.row -->

  </div>
  <!-- /.container -->

  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Your Website 2019</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
