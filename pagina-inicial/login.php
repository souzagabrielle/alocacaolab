<?php
//Iniciando a sessão:
if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
  }
  //Gravando valores dentro da sessão aberta:
  $_SESSION['user'] = 'admin';
  $_SESSION['pass'] = 'admin';

  ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Alocação de laboratórios</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="res/site/fonts/material-icon/css/material-design-iconic-font.min.css">

    <!-- Main css -->
    <link rel="stylesheet" href="res/site/css/style.css">

</head>
<body>
    
            <div class="container">
                <div class="signin-content">
                    <div class="signin-image">
                        <figure><img src="res/site/images/signin-image.jpg" alt="sing up image"></figure>
                        <a href="cadastro.html" class="signup-image-link">Criar uma conta</a>
                        <a href="index.html" class="signup-image-link">Voltar ao site</a>
                    </div>

            
                    <div class="signin-form">
                        <h2 class="form-title">Sign up</h2>

                    <?php 
                    if(!$_POST){
                         echo "
                        <form method='post' class='register-form' id='login-form' action='login.php'>
                            <div class='form-group'>
                                <label for='user'><i class='zmdi zmdi-email'></i></label>
                                <input type='user' name='user'  placeholder='Seu usuario'/>
                            </div>
                            <div class='form-group'>
                                <label for='pass'><i class='zmdi zmdi-lock'></i></label>
                                <input type='password' name='pass'  placeholder='Senha'/>
                            </div>
                            <div class='form-group'>
                                <input type='checkbox' name='lembrar' id='lembrar' class='agree-term' />
                                <label for='lembrar' class='label-agree-term'><span><span></span></span>Lembrar a senha.</label>
                            </div>
                            <div class='form-group form-button'>
                                <input type='submit' name='login' id='login' class='form-submit' value='Entrar'/>
                            </div>
                            </form>
                            </div>
                          </div>
                      </div>
                     ";

                
                    }else{
                        $user = $_POST['user']; 
                        $pass = $_POST['pass'];
                        
                        if (empty($_POST['user']) || empty($_POST['pass'])) {
                            echo  "Campo(s) vazio(s)!";
                    
                        }
                    
                       elseif ($user == 'admin' && $pass == "admin") {
                            header('Location: ../dashboard/examples/dashboard.html');
                        }
                        else {
                        echo("Senha errada, acesso não permitido!");
                        }
                    
                    }
                        
                        
     ?>
                        

        <!-- codigo php-->

    <!-- JS -->
    <script src="res/site/vendor/jquery/jquery.min.js"></script>
    <script src="res/site/js/main.js"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>